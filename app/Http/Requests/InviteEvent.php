<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InviteEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'users' => 'required|array'
        ];
        foreach ($this->request->get('users') as $key => $val) {
            $rules['users.'.$key] = 'required|numeric|exists:users,id';
        }

        return $rules;
    }
}
