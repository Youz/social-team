<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Event extends Model
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function routeNotificationForDiscord()
    {
        return $this->discord_channel;
    }

    /**
     * The roles that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_event');
    }

    public function organizer()
    {
        return $this->belongsToMany(User::class, 'user_event')->where('isOrganizer', true)->firstOrFail();
    }
}
