<?php

namespace Tests\Feature;

use App\Event;
use App\Notifications\EventDiscord;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class EventTest extends TestCase
{
    /** @test */
    public function a_user_can_create_an_event()
    {
        $event = factory(Event::class)->make();
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->post('api/event', $event->toArray());

        //Get event created to get the id
        $dbEvent = Event::first();
        $response->assertStatus(201);
        $this->assertDatabaseHas('events', ['name' => $event->name, 'description' => $event->description]);
        $this->assertDatabaseHas('user_event',
            ['event_id' => $dbEvent->id, 'user_id' => $user->id, 'status' => 'accepted', 'isOrganizer' => true]);
        $response->assertJson($event->toArray());
    }

    /** @test */
    public function a_user_can_fetch_events()
    {
        $user = factory(User::class)->create();
        $firstEvent = factory(Event::class)->create();
        $secondEvent = factory(Event::class)->create();

        $response = $this->actingAs($user)->get('api/event');

        $response->assertStatus(200);
        $response->assertJsonCount(2);
        $response->assertJson([
            ['name' => $firstEvent->name, 'description' => $firstEvent->description],
            ['name' => $secondEvent->name, 'description' => $secondEvent->description],
        ]);
        $response->assertJsonStructure(['*' => $this->getJsonStructure()]);
    }

    /** @test */
    public function a_user_can_show_an_event()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create();

        $response = $this->actingAs($user)->get('api/event/' . $event->id);

        $response->assertStatus(200);
        $response->assertJsonFragment($event->toArray());
        $response->assertJsonStructure([
            '*' => $this->getJsonStructure(),
        ]);
    }

    /** @test */
    public function a_user_can_update_his_event()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create(['name' => 'Original Name', 'description' => 'Original Description']);
        $event->users()->attach($user, ['isOrganizer' => true]);

        $form = ['name' => 'Updated Name', 'description' => 'Updated Description'];
        $response = $this->actingAs($user)->put('api/event/' . $event->id, $form);

        $response->assertStatus(200);
        $this->assertDatabaseHas('events', $form);
        $this->assertDatabaseMissing('events', $event->toArray());
        $response->assertJsonFragment($form);
    }

    /** @test */
    public function a_user_cant_update_an_event_if_he_is_not_organizer()
    {
        $user = factory(User::class)->create();
        $wrongUser = factory(User::class)->create();
        $event = factory(Event::class)->create(['name' => 'Original Name', 'description' => 'Original Description']);
        $event->users()->attach($user, ['isOrganizer' => true]);

        $form = ['name' => 'Updated Name', 'description' => 'Updated Description'];
        $response = $this->actingAs($wrongUser)->put('api/event/' . $event->id, $form);

        $response->assertStatus(401);
        $this->assertDatabaseMissing('events', $form);
        $this->assertDatabaseHas('events', $event->toArray());
    }

    /** @test */
    public function a_user_can_delete_his_event()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create();
        $event->users()->attach($user->id, ['isOrganizer' => true]);

        $response = $this->actingAs($user)->delete('api/event/' . $event->id);

        $response->assertStatus(200);
        $this->assertDatabaseMissing('events', $event->toArray());
    }

    /** @test */
    public function a_user_cant_delete_event_if_he_is_not_organizer()
    {
        $user = factory(User::class)->create();
        $wrongUser = factory(User::class)->create();
        $event = factory(Event::class)->create();
        $event->users()->attach($user->id, ['isOrganizer' => true]);

        $response = $this->actingAs($wrongUser)->delete('api/event/' . $event->id);

        $response->assertStatus(401);
        $this->assertDatabaseHas('events', $event->toArray());
    }

    /** @test */
    public function a_user_can_invite_others_users_to_his_event()
    {
        Notification::fake();

        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $user3 = factory(User::class)->create();
        $event = factory(Event::class)->create(['discord_channel' => '651853303382147093']);
        $event->users()->attach($user->id, ['isOrganizer' => true, 'status' => 'accepted']);

        $form = ['users' => [2, 3]];
        $response = $this->post('api/event/' . $event->id . '/invite/', $form);

        $response->assertStatus(201);
        Notification::assertSentTo([$event], EventDiscord::class);
        $this->assertDatabaseHas('user_event',
            ['event_id' => $event->id, 'user_id' => $user->id, 'status' => 'accepted', 'isOrganizer' => true]);
        $this->assertDatabaseHas('user_event',
            ['event_id' => $event->id, 'user_id' => $user2->id, 'status' => 'created', 'isOrganizer' => false]);
        $this->assertDatabaseHas('user_event',
            ['event_id' => $event->id, 'user_id' => $user3->id, 'status' => 'created', 'isOrganizer' => false]);
    }

    /** @test */
    public function a_user_can_see_all_events_were_he_is_invited()
    {
        $user = factory(User::class)->create();
        $event1 = factory(Event::class)->create();
        $event1->users()->attach($user->id);
        $event2 = factory(Event::class)->create();
        $event2->users()->attach($user->id);
        $event3 = factory(Event::class)->create();
        $event3->users()->attach($user->id);

        $response = $this->actingAs($user)->get('/invitations');

        $response->assertStatus(200);
        $response->assertSee($event1->name);
        $response->assertSee($event2->name);
        $response->assertSee($event3->name);
        $response->assertViewIs('events.invitations');
    }

    /** @test */
    public function a_user_can_accept_ot_refuse_an_invitation()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $event = factory(Event::class)->create();
        $event->users()->attach($user->id, ['status' => 'sent']);
        $event->users()->attach($user2->id, ['status' => 'accepted', 'isOrganizer' => true]);

        $form = ['status' => 'accepted'];
        $response = $this->actingAs($user)->put('api/event/' . $event->id . '/invite', $form);

        $response->assertStatus(200);
        $this->assertDatabaseHas('user_event', ['user_id' => $user->id, 'status' => 'accepted']);
    }

    /**
     * @return array
     */
    private function getJsonStructure(): array
    {
        return ['id', 'name', 'description', 'created_at', 'updated_at'];
    }
}
