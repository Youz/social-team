@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @forelse ($events as $event)
                    {{ $event->name }}
                    {{ $event->invitations }}
                @empty
                    <p>No Events</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
