[![StyleCI](https://gitlab.styleci.io/repos/15387671/shield?branch=master)](https://gitlab.styleci.io/repos/15387671)
#Social Team

## Installation
 - Git clone from repo.
 - docker-compose up -d
 
 ## First Run
 When all containers are up we need to add 777 to `storage`, in a term in project dir :
 
 `docker exec -it app_social_team bash`
 Now you should be in ssh in app container.
 
 `chmod -R 777 storage/`
 
In your **local** project dir (not in your container)
 
 `php artisan key:generate`
 
Then check your `.env` use `.env.example` for default values.
 
 ## Daily use
 
 Start : `docker-compose up -d`
 Stop : `docker-compose halt`
 
 Link to local app:
 [http://localhost:8080](http://localhost:8080)
 
 Link to PhpMyAdmin : 
  [http://localhost:8081](http://localhost:8081)
  root/root

## Code contribution

Branch master = Prod
Branch dev = Pre prod (or last version stable)

Config the dev branch :
`git branch --set-upstream-to=origin/dev dev`

How to work :
Create new branch named by the feature you want to add (example `git branch -b authentification` or `git branch -b fixing-{ticketNumber}` )

You must add unit test for your PR.
When you'r done, make a PR to `dev` branch. Don't forget to delete branch on Gitlab when your PR is accepted.

If you add a env variable in `.env`, add it to `.env.example`
You must send a PR only if all tests are green.


## Links

Discord package
https://github.com/laravel-notification-channels/discord
