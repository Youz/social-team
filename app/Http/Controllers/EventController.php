<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\Appearance;
use App\Http\Requests\InviteEvent;
use App\Http\Requests\StoreEvent;
use App\Notifications\EventDiscord;

class EventController extends Controller
{
    public function index()
    {
        return response()->json(Event::all(), 200);
    }

    public function show(Event $event)
    {
        return response()->json(Event::findOrFail($event), 200);
    }

    public function store(StoreEvent $request)
    {
        $event = new Event();
        $event->name = $request->get('name');
        $event->description = $request->get('description');
        $isSaved = $event->save();

        if (! $isSaved) {
            return response()->json('Something went wrong', 422);
        }

        //Attach default invitation to organizer
        $event->users()->attach(auth()->user()->id, [
            'status'      => 'accepted',
            'isOrganizer' => true
        ]);

        return response()->json($event, 201);
    }

    public function update(StoreEvent $request, Event $event)
    {
        if ($event->organizer()->id !== auth()->user()->id) {
            return response()->json('You are not authorized to edit this event', 401);
        }

        $isUpdated = $event->update($request->all());

        if (! $isUpdated) {
            return response()->json('Something get wrong during event update', 422);
        }

        return response()->json($event);
    }

    public function delete(Event $event)
    {
        if ($event->organizer()->id !== auth()->user()->id) {
            return response()->json('You are not authorized to edit this event', 401);
        }

        $isDeleted = $event->delete();

        if (! $isDeleted) {
            return response()->json('Something get wrong during event deleting', 422);
        }

        return response()->json('Event has been deleted');
    }

    public function invite(InviteEvent $request, Event $event)
    {
        $event->users()->attach($request->get('users'));
        $event->notify(new EventDiscord($event));

        return \response()->json('Users will be invited', 201);
    }

    public function invitations()
    {
        $events = Auth()->user()->events;

        return view('events.invitations', ['events' => $events]);
    }

    public function appearance(Appearance $request, Event $event)
    {
        $user = Auth()->user();
        if ($user->events()->updateExistingPivot($event->id, ['status'=>$request->get('status')])) {
            return \response()->json('User\'s invitation updated', 200);
        }

        return response()->json('Something got wrong', 422);
    }
}
