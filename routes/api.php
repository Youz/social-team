<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Events
 */
Route::get('/event', 'EventController@index');
Route::get('/event/{event}', 'EventController@show');
Route::post('/event', 'EventController@store');
Route::put('/event/{event}', 'EventController@update');
Route::delete('/event/{event}', 'EventController@delete');
Route::post('/event/{event}/invite', 'EventController@invite');
Route::put('/event/{event}/invite', 'EventController@appearance');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
