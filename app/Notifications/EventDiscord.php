<?php

namespace App\Notifications;

use App\Event;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Discord\DiscordChannel;
use NotificationChannels\Discord\DiscordMessage;

class EventDiscord extends Notification
{
    use Queueable;
    /**
     * @var Event
     */
    public $event;

    /**
     * Create a new notification instance.
     *
     * @param Event $event
     * @param User $user
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DiscordChannel::class];
    }

    public function toDiscord($notifiable)
    {
        return DiscordMessage::create($this->getMessage());
    }

    private function getMessage()
    {
        $link = env('APP_URL').'/invitations';
        $messages = [
            "A event of __**{$this->event->name}**__ by **{$this->event->organizer()->fullName}** has been created!".PHP_EOL."Accept or decline your invitation : $link",
            "**{$this->event->organizer()->fullName}** want some friends to go to __**{$this->event->name}**__".PHP_EOL."Accept or decline if you are not his friend : $link",
            "GO GO GO __**{$this->event->name}**__ with **{$this->event->organizer()->fullName}** the magnificent!".PHP_EOL."Cast your response here : $link",
            "Guy of the month **{$this->event->organizer()->fullName}** invited you to __**{$this->event->name}**__ !".PHP_EOL."Would you go ? : $link",
            "Is that kickin ? No only **{$this->event->organizer()->fullName}** who want you to join __**{$this->event->name}**__ ! Say your words here : ".PHP_EOL.$link,
            "**{$this->event->organizer()->fullName}** want socialize you in __**{$this->event->name}**__ respond yes or no here : ".PHP_EOL."$link",
        ];

        return $messages[rand(0, count($messages) - 1)];
    }
}
